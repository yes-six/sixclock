#include "AppManager.h"
static const uint8_t settings_bits[] = {
    0x00, 0x00, 0xf0, 0x00, 0x00, 0x00, 0xfc, 0x03, 0x00, 0x00, 0xff, 0x03,
    0x00, 0x80, 0xc7, 0x03, 0x00, 0xc0, 0xe3, 0x01, 0x00, 0xc0, 0xf1, 0x00,
    0x00, 0xc0, 0x78, 0x70, 0x00, 0xe0, 0x38, 0x78, 0x00, 0xe0, 0x38, 0xfc,
    0x00, 0x60, 0x30, 0xfe, 0x00, 0xe0, 0xf0, 0xef, 0x00, 0xe0, 0xf0, 0xe7,
    0x00, 0xe0, 0x80, 0x63, 0x00, 0x70, 0x00, 0x70, 0x00, 0x38, 0x00, 0x38,
    0x00, 0x1c, 0x00, 0x3c, 0x00, 0x0e, 0xb8, 0x1f, 0x00, 0x07, 0xfc, 0x0f,
    0x80, 0x03, 0xfe, 0x01, 0xc0, 0x01, 0x07, 0x00, 0xe0, 0x80, 0x03, 0x00,
    0x70, 0xc0, 0x01, 0x00, 0x38, 0xe0, 0x00, 0x00, 0x1c, 0x70, 0x00, 0x00,
    0x0e, 0x38, 0x00, 0x00, 0x07, 0x1c, 0x00, 0x00, 0x27, 0x0e, 0x00, 0x00,
    0x07, 0x07, 0x00, 0x00, 0x86, 0x03, 0x00, 0x00, 0xfe, 0x01, 0x00, 0x00,
    0xfc, 0x00, 0x00, 0x00, 0x70, 0x00, 0x00, 0x00};

static const menu_item settings_menu_main[] =
    {
        {NULL, "退出"},
        {NULL, "时间设置"},
        {NULL, "闹钟设置"},
        {NULL, "天气设置"},
        {NULL, "网络设置"},
        {NULL, "系统设置"},
        {NULL, "检测外设"},
        {NULL, "关于本机"},
        {NULL, NULL},
};

static const menu_item settings_menu_time[] =
    {
        {NULL, "返回上一级"},
        {NULL, "立即进行NTP对时"},
        {NULL, "时间同步间隔设置"},
        {NULL, "查看RTC偏移修正"},
        {NULL, "使用离线模式"},
        {NULL, NULL},
};

static const menu_item settings_menu_Weather[] =
    {
        {NULL, "返回上一级"},
        {NULL, "天气更新间隔"},
        {NULL, "立即更新天气"},
        {NULL, NULL},
};

static const menu_item settings_menu_network[] =
    {
        {NULL, "返回上一级"},
        {NULL, "ESPTouch配网"},
        {NULL, "启动网页服务器"},
        {NULL, "ESPNow设备扫描(正在开发中)"},
        {NULL, "蓝牙扫描(正在开发中)"},
        {NULL, "退出Bilibili账号"},
        {NULL, NULL},
};

static const menu_item settings_menu_peripheral[] =
    {
        {NULL, "返回上一级"},
        {NULL, "重新检测外设"},
        {NULL, "AHT20"},
        {NULL, "BMP280"},
        {NULL, "SGP30"},
        {NULL, "DS3231"},
        {NULL, "SD卡"},
        {NULL, NULL},
};

static const menu_item settings_menu_system[] =
    {
        {NULL, "返回上一级"},
        {NULL, "屏幕方向设置"},
        {NULL, "电池校准"},
        {NULL, "主屏幕应用选择"},
        {NULL, "应用安装管理"},
        {NULL, "电源选项"},
        {NULL, "恢复出厂设置"},
        {NULL, NULL},
};

static const menu_item settings_menu_about[] =
    {
        {NULL, "返回上一级"},
        {NULL, "设备型号: Six-Clock"},
        {NULL, "微处理器: ESP32-WROOM-32"},
        {NULL, "屏幕参数: E-Paper 2.9In 296x128P"},
        {NULL, "电池容量: 600/1500mAh(typ)"},
        {NULL, "查看设备ID"},
        {NULL, "查看硬件版本"},
        {NULL, "查看软件版本"},
        {NULL, "查看芯片型号"},
        {NULL, "查看使用环境"},
        {NULL, "查看产品说明书"},
        {NULL, NULL},
};

class AppSettings : public AppBase
{
private:
    String toApp = "";
    bool hasToApp = false;

public:
    AppSettings()
    {
        name = "settings";
        title = "设置";
        description = "简单的设置";
        image = settings_bits;
        noDefaultEvent = true;
    }
    void setup();
    void menu_time();
    void menu_alarm();
    void menu_weather();
    void menu_network();
    void menu_peripheral();
    void menu_system();
    void menu_about();
};
static AppSettings app;

void AppSettings::setup()
{
    display.clearScreen();
    GUI::drawWindowsWithTitle("设置", 0, 0, 296, 128);
    u8g2Fonts.drawUTF8(120, 75, "请稍等...");

    int res = 0;
    bool end = false;
    while (end == false && hasToApp == false)
    {
        display.display(false);
        res = GUI::menu("设置", settings_menu_main);
        switch (res)
        {
        case 0:
            end = true;
            break;
        case 1:

            menu_time();
            break;
        case 2:

            menu_alarm();
            break;
        case 3:

            menu_weather();
            break;
        case 4:

            menu_network();
            break;
        case 5:

            menu_system();
            break;
        case 6:

            peripherals.check();
            break;
        case 7:

            menu_about();
            break;
        default:
            break;
        }
    }
    if (hasToApp == true)
    {
        hasToApp = false;
        if (toApp != "")
        {
            appManager.gotoApp(toApp.c_str());
        }
        return;
    }
    appManager.goBack();
}

void AppSettings::menu_time()
{
    int res = 0;
    bool end = false;
    while (end == false && hasToApp == false)
    {
        res = GUI::menu("时间设置", settings_menu_time);
        switch (res)
        {
        case 0:
            end = true;
            break;
        case 1:

            if (GUI::msgbox_yn("立即进行NTP对时", "即将连接WiFi并同步时间") == true)
            {

                GUI::drawWindowsWithTitle("立即进行NTP对时", (296 - 160) / 2, (128 - 96) / 2, 160, 96);
                u8g2Fonts.setCursor(71, 16 + 30);
                u8g2Fonts.print("正在连接网络...");
                Serial.println("ntp即将连接网络...");
                display.display(true);
                hal.autoConnectWiFi();
                Serial.println("ntp已连接网络...");
                u8g2Fonts.setCursor(71, 16 + 30);
                u8g2Fonts.print("网络连接成功");
                display.display(true);
                u8g2Fonts.setCursor(71, 16 + 30 + 15);
                u8g2Fonts.print("正在对时...");
                display.display(true);
                Serial.println("ntp即将对时...");

                NTPSync();
                GUI::msgbox("立即进行NTP对时", "同步完成");
            }
            break;
        case 2:

        {
            const menu_item menu[] = {
                {NULL, "取消"},
                {NULL, "禁用时间同步"},
                {NULL, "2小时"},
                {NULL, "4小时"},
                {NULL, "6小时"},
                {NULL, "12小时"},
                {NULL, "24小时"},
                {NULL, "36小时"},
                {NULL, "48小时"},
                {NULL, NULL},
            };
            res = GUI::menu("时间同步间隔设置", menu);
            if (res > 0)
            {

                hal.pref.putUChar(SETTINGS_PARAM_NTP_INTERVAL, res - 1);
                GUI::msgbox("时间同步间隔设置", "设置完成");
            }
        }
        break;
        case 3:

            toApp = "rtcoffset";
            hasToApp = true;
            end = true;
            break;
        case 4:

            if (GUI::msgbox_yn("时钟样式选择", "请选择使用天气时钟还是仅时钟", "天气时钟", "仅时钟"))
            {

                config[PARAM_CLOCKONLY] = "0";
                hal.saveConfig();
            }
            else
            {
                config[PARAM_CLOCKONLY] = "1";
                hal.saveConfig();
            }
        default:
            break;
        }
    }
}

void AppSettings::menu_alarm()
{
    int res = 0;
    bool end = false;
    menu_item settings_menu_alarm[] = {
        {NULL, "返回上一级"},
        {NULL, NULL},
        {NULL, NULL},
        {NULL, NULL},
        {NULL, NULL},
        {NULL, NULL},
        {NULL, "闹钟铃声"},
        {NULL, NULL},
    };
    const menu_item settings_menu_alarm_sub[] = {
        {NULL, "返回"},
        {NULL, "时间"},
        {NULL, "重复周期"},
        {NULL, NULL},
    };
    const menu_item settings_menu_alarm_time[] = {
        {NULL, "返回"},
        {NULL, "关闭"},
        {NULL, "单次"},
        {NULL, "周一到周五"},
        {NULL, "周六日"},
        {NULL, "周一"},
        {NULL, "周二"},
        {NULL, "周三"},
        {NULL, "周四"},
        {NULL, "周五"},
        {NULL, "周六"},
        {NULL, "周日"},
        {NULL, "手动输入"},
        {NULL, NULL},
    };
    char alarm_buf[5][30];
    char alarm_buf_week[25];
    char bit_week[7] = {0};
    while (end == false && hasToApp == false)
    {

        for (int i = 0; i < 5; ++i)
        {
            if (alarms.alarm_table[i].enable == 0)
            {
                sprintf(alarm_buf[i], "%d：%02d:%02d，关闭", i + 1, alarms.alarm_table[i].time / 60, alarms.alarm_table[i].time % 60, alarms.getEnable(alarms.alarm_table + i).c_str());
            }
            else
            {
                sprintf(alarm_buf[i], "%d：%02d:%02d,%s", i + 1, alarms.alarm_table[i].time / 60, alarms.alarm_table[i].time % 60, alarms.getEnable(alarms.alarm_table + i).c_str());
            }
            settings_menu_alarm[i + 1].title = alarm_buf[i];
        }
        res = GUI::menu("闹钟设置", settings_menu_alarm);
        if (res == 0)
            break;
        if (res == 6)
        {
            const char *str = GUI::fileDialog("请选择闹钟铃声文件", false, "buz");
            if (str)
            {
                hal.pref.putString(SETTINGS_PARAM_ALARM_TONE, String(str));
            }
            else
            {
                if (GUI::msgbox_yn("你选择了返回", "是否使用默认铃声，或者保留之前的设置", "使用默认", "取消"))
                {
                    hal.pref.remove(SETTINGS_PARAM_ALARM_TONE);
                }
            }
        }
        int selected = res - 1;
        res = GUI::menu(alarm_buf[selected], settings_menu_alarm_sub);
        switch (res)
        {
        case 0:
            break;
        case 1:
        {
            alarms.alarm_table[selected].time = GUI::msgbox_time("请输入闹钟时间", alarms.alarm_table[selected].time);
            if (alarms.alarm_table[selected].enable == 0)
                alarms.alarm_table[selected].enable = ALARM_ENABLE_ONCE;
            break;
        }
        case 2:
        {
            int res;
            res = GUI::menu("请选择重复周期", settings_menu_alarm_time);
            enum alarm_enable_enum res_table[] = {
                ALARM_DISABLE,
                ALARM_ENABLE_ONCE,
                (enum alarm_enable_enum)0b00111110,
                (enum alarm_enable_enum)0b01000001,
                ALARM_ENABLE_MONDAY,
                ALARM_ENABLE_TUESDAY,
                ALARM_ENABLE_WEDNESDAY,
                ALARM_ENABLE_THURSDAY,
                ALARM_ENABLE_FRIDAY,
                ALARM_ENABLE_SATDAY,
                ALARM_ENABLE_SUNDAY,
            };
            switch (res)
            {
            case 0:
                break;
            case 12:
            {
                int time = GUI::msgbox_number("输入重复周期Bitmap", 3, alarms.alarm_table[selected].enable);
                alarms.alarm_table[selected].enable = (enum alarm_enable_enum)(time % 256);
            }
            break;
            default:
                alarms.alarm_table[selected].enable = (enum alarm_enable_enum)(res_table[(res - 1) % 11]);
                break;
            }
        }
        break;
        default:
            break;
        }
    }
    alarms.save();
}

void AppSettings::menu_weather()
{
    int res = 0;
    bool end = false;
    while (end == false && hasToApp == false)
    {
        res = GUI::menu("天气设置", settings_menu_Weather);
        switch (res)
        {
        case 0:
            end = true;
            break;
        case 1:

        {
            int res = GUI::msgbox_number("请输入天气更新间隔", 2, atoi(config[PARAM_FULLUPDATE].as<const char *>()));
            if (res <= 5 || res > 55)
            {
                res = 55;
            }
            char tmp[4];
            sprintf(tmp, "%d", res);
            config[PARAM_FULLUPDATE] = tmp;
            hal.saveConfig();
            break;
        }
        case 2:

            if (GUI::msgbox_yn("立即更新天气", "即将连接WiFi并更新天气") == true)
            {

                GUI::drawWindowsWithTitle("立即更新天气", (296 - 160) / 2, (128 - 96) / 2, 160, 96);
                u8g2Fonts.setCursor(71, 16 + 30);
                u8g2Fonts.print("正在更新天气信息...");
                display.display();
                switch (weather.refresh())
                {
                case 0:
                    GUI::msgbox("提示", "天气信息更新成功");
                    break;
                case -2:
                    GUI::msgbox("提示", "更新失败，网络异常，请检查");
                    break;
                case -3:
                    GUI::msgbox("提示", "更新失败，API异常，请检查");
                    break;
                default:
                    break;
                }

                break;
            }
            else
                break;
        default:
            break;
        }
    }
}

void AppSettings::menu_network()
{
    int res = 0;
    bool end = false;
    while (end == false && hasToApp == false)
    {
        res = GUI::menu("网络设置", settings_menu_network);
        switch (res)
        {
        case 0:
            end = true;
            break;
        case 1:

            hal.WiFiConfigSmartConfig();
            break;
        case 2:

            toApp = "webserver";
            hasToApp = true;
            end = true;
            break;
        case 3:

            break;
        case 4:

            break;
        case 5:

            if (LittleFS.exists("/blCookies.txt"))
            {
                LittleFS.remove("/blCookies.txt");
                GUI::msgbox("完成", "Bilibili 已退出");
                break;
            }
            else
            {
                GUI::msgbox("提示", "Bilibili 当前未登陆");
                break;
            }
        default:
            break;
        }
    }
}

#include <nvs_flash.h>

void AppSettings::menu_system()
{
    int res = 0;
    bool end = false;
    while (end == false && hasToApp == false)
    {
        res = GUI::menu("系统设置", settings_menu_system);
        switch (res)
        {
        case 0:
            end = true;
            break;
        case 1:

            if (GUI::msgbox_yn("屏幕方向设置", "按钮在右上方为默认，按钮请拨至右侧\n按钮在左下方为翻转，按钮请拨至右左侧\n请选择...", "默认（右）", "翻转（左）"))
            {
                hal.pref.putUChar(SETTINGS_PARAM_SCREEN_ORIENTATION, 3);
            }
            else
            {
                hal.pref.putUChar(SETTINGS_PARAM_SCREEN_ORIENTATION, 1);
            }
            display.display();
            GUI::msgbox("提示", "按键控制方式请修改GPIO宏定义");
            break;
        case 2:

            if (GUI::msgbox_yn("电池校准", "请确认已将电池充满电(建议充电3小时以上)，并移除充电线，若满电电压偏大请减小数值，反之则增加") == true)
            {

                int res = GUI::msgbox_number("请输入ADC校准值...", 4, atoi(config[PARAM_BATV].as<const char *>()));
                if (res <= 6600 || res > 8000)
                {
                    res = 6600;
                }
                char tmp[4];
                sprintf(tmp, "%d", res);
                config[PARAM_BATV] = tmp;
                hal.saveConfig();
                break;

                break;
            }
            else
                break;
        case 3:

        {
            AppBase *tmp = appManager.appSelector(true);
            if (tmp)
            {
                Serial.println(tmp->name);
                if (GUI::msgbox_yn("警告", "选择不兼容的App可能会导致无法开机，是否确认？\n请谨慎，切勿随意选择") == true)
                {
                    if (strcmp(tmp->name, "clock") == 0)
                    {
                        config[PARAM_CLOCKONLY] = "0";
                        hal.saveConfig();
                        hal.pref.putString(SETTINGS_PARAM_HOME_APP, "clock");
                    }
                    else if (strcmp(tmp->name, "clockonly") == 0)
                    {
                        config[PARAM_CLOCKONLY] = "0";
                        hal.saveConfig();
                        hal.pref.putString(SETTINGS_PARAM_HOME_APP, "clockonly");
                    }
                    else
                    {
                        hal.pref.putString(SETTINGS_PARAM_HOME_APP, tmp->name);
                    }
                    GUI::msgbox("设置成功", "重启或下次唤醒后生效");
                }
            }
            break;
        }
        case 4:
            toApp = "installer";
            hasToApp = true;
            end = true;
            return;
            break;
        case 5:
            toApp = "power";
            hasToApp = true;
            end = true;
            return;
            break;
        case 6:

        {
            if (GUI::msgbox_yn("此操作不可撤销", "是否恢复出厂设置？"))
            {
                if (GUI::msgbox_yn("这是最后一次提示", "将格式化nvs和LittleFS存储区", "取消（右）", "确认（左）") == false)
                {
                    GUI::drawWindowsWithTitle("恢复出厂设置", (296 - 160) / 2, (128 - 96) / 2, 160, 96);

                    display.clearScreen();
                    u8g2Fonts.setCursor(71, 16 + 30);
                    u8g2Fonts.print("正在格式化NVS存储空间...");
                    display.display(true);
                    nvs_flash_erase();
                    u8g2Fonts.print("OK");
                    display.display(true);
                    u8g2Fonts.setCursor(71, 16 + 30 + 15);
                    u8g2Fonts.print("正在格式化LittleFS存储空间...");
                    display.display(true);
                    LittleFS.end();
                    LittleFS.format();
                    u8g2Fonts.print("OK");
                    display.display(true);
                    u8g2Fonts.setCursor(71, 16 + 30 + 15 + 15);
                    u8g2Fonts.print("已恢复出厂设置，正在重启");
                    display.display(true);
                    ESP.restart();
                }
            }
        }
        break;
        default:
            break;
        }
    }
}

void AppSettings::menu_about()
{
    int res = 0;
    bool end = false;
    uint32_t chipid = 0;
    char ChipID[30];
    char ChipMV[30];
    char SoftMV[30];

    while (end == false && hasToApp == false)
    {
        res = GUI::menu("关于本机", settings_menu_about);
        switch (res)
        {
        case 0:
            end = true;
            break;
        case 5:
        {
            for (int i = 0; i < 17; i = i + 8)
            {
                chipid |= ((ESP.getEfuseMac() >> (40 - i)) & 0xff) << i;
            }
            sprintf(ChipID, "%d", chipid);
            GUI::msgbox("设备ID", ChipID);
            break;
        }
        case 6:
        {
            GUI::msgbox("硬件版本", "V2.0");
            break;
        }

        case 7:
        {
            sprintf(SoftMV, "更新于\n" __DATE__ " " __TIME__, chipid);
            GUI::msgbox("软件版本", SoftMV);
            break;
        }
        case 8:
        {
            sprintf(ChipMV, "%s Rev %d", ESP.getChipModel(), ESP.getChipRevision());
            GUI::msgbox("芯片版本", ChipMV);
            break;
        }
        case 9:
        {

            GUI::msgbox("使用环境", "温度:0~40°C\n湿度:35%~65%RH\n使用场所:室内");
            break;
        }
        case 10:
        {
            QRCode qrcode;
            uint8_t qrcodeData[qrcode_getBufferSize(7)];
            GUI::drawWindowsWithTitle("产品说明书", 0, 0, 296, 128);
            qrcode_initText(&qrcode, qrcodeData, 6, 0, "https://gitee.com/yes-six/sixclock/wikis");
            Serial.println(qrcode.size);
            for (uint8_t y = 0; y < qrcode.size; y++)
            {

                for (uint8_t x = 0; x < qrcode.size; x++)
                {
                    display.fillRect(2 * x + 20, 2 * y + 30, 2, 2, qrcode_getModule(&qrcode, x, y) ? GxEPD_BLACK : GxEPD_WHITE);
                }
            }
            u8g2Fonts.setCursor(140, 40);
            u8g2Fonts.print("产品说明书");
            u8g2Fonts.setCursor(140, 55);
            u8g2Fonts.print("请扫描左侧二维码阅读");
            u8g2Fonts.setCursor(140, 70);
            u8g2Fonts.print("本页可在设置-关于本机查询");
            u8g2Fonts.setCursor(140, 85);
            u8g2Fonts.print("请拨动任意键继续");
            display.display();
            hal.waitClick();
            break;
        }
        default:
            end = false;
            break;
        }
    }
}