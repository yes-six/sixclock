#include "AppManager.h"

static const uint8_t clock_icon[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0xfc, 0x7f, 0x00, 0x00, 0xff, 0xff, 0x00, 0xc0, 0x07, 0xc0, 0x03,
    0xe0, 0x01, 0x00, 0x07, 0x70, 0x80, 0x01, 0x0c, 0x38, 0x80, 0x01, 0x18, 0x18, 0x80, 0x01, 0x18,
    0x1c, 0x80, 0x01, 0x30, 0x0c, 0x80, 0x01, 0x70, 0x0e, 0x80, 0x01, 0x60, 0x06, 0x80, 0x01, 0x60,
    0x06, 0x80, 0x01, 0x60, 0x06, 0x80, 0x01, 0x60, 0x06, 0x80, 0x01, 0x60, 0x06, 0x80, 0x01, 0x66,
    0xc6, 0x83, 0x03, 0x66, 0x66, 0x06, 0x07, 0x60, 0x36, 0x0c, 0x0e, 0x60, 0x16, 0x18, 0x1c, 0x60,
    0x16, 0x10, 0x18, 0x60, 0x1e, 0x78, 0x00, 0x60, 0x0c, 0xcc, 0x00, 0x70, 0x04, 0x8c, 0x01, 0x30,
    0x06, 0x00, 0x01, 0x18, 0x06, 0x00, 0x01, 0x18, 0x06, 0x80, 0x01, 0x0c, 0x0e, 0xc0, 0x00, 0x07,
    0x1c, 0x60, 0xc0, 0x03, 0xf8, 0xff, 0xff, 0x00, 0xf0, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00};

const char *dayOfWeek[] = {"日", "一", "二", "三", "四", "五", "六"};
RTC_DATA_ATTR int part_refresh_count = 0;
RTC_DATA_ATTR bool force_full_update = false;
RTC_DATA_ATTR int last_alertPubTime = 0;
static void appclock_exit()
{
    force_full_update = true;
}
static void appclock_deepsleep()
{
}

class AppClock : public AppBase
{
private:
public:
    AppClock()
    {
        name = "clock";
        title = "天气时钟";
        description = "默认主页应用";
        image = clock_icon;
        _showInList = true;
    }
    void setup();

private:
    inline int getMinuteShift(int minute)
    {
        return minute * PX_PER_SAMPLE / 60;
    }
    void drawWeatherIcon(int pos, int code, int y = 90)
    {
        display.drawXBitmap(pos, y, weather_icons_day[code].data, weather_icons_day[code].width, weather_icons_day[code].height, GxEPD_BLACK);
    }

    void drawPlot(int hour = 0, int minute = 0)
    {
        int minute_shift = getMinuteShift(minute);
        display.fillRect(0, 122, SCREEN_WIDTH, 6, GxEPD_BLACK);
        display.setFont(&Picopixel);
        display.setTextColor(GxEPD_WHITE);
        for (int i = 0; i < SAMPLE_COUNT; i++)
        {
            int x = (i * SCREEN_WIDTH) / 8 - minute_shift;
            display.setCursor(x, 127);
            display.printf("%02d:00", (i + hour) % 24);
        }
        display.setTextColor(GxEPD_BLACK);

        uint16_t rainmax = 0;
        for (int i = 0; i < SAMPLE_COUNT; ++i)
        {
            if (weather.hour24[i + hal.global_hour_offset].rain > rainmax)
                rainmax = weather.hour24[i + hal.global_hour_offset].rain;
            rain_data_raw[i] = weather.hour24[i + hal.global_hour_offset].rain;
        }
        if (rainmax < 500)
            rainmax = 500;
        processRain((float)(rainmax));

        display.fillRect(0, 122 - GRAPH_HEIGHT, SCREEN_WIDTH, GRAPH_HEIGHT, GxEPD_WHITE);
        for (int i = 0; i < SCREEN_WIDTH; ++i)
        {

            if (ydata[i + minute_shift] != 0)
            {
                display.drawPixel(i, 122 - ydata[i + minute_shift], GxEPD_BLACK);
                for (int j = 122 - ydata[i + minute_shift]; j < 122; j++)
                {
                    if ((i % 2) == (j % 2))
                        display.drawPixel(i, j, GxEPD_BLACK);
                }
            }
        }

        const void *last_symb = weather_icons_day[weather.hour24[hal.global_hour_offset].weathernum].data;
        int i = 0;
        if ((last_symb == weather_icons_day[weather.hour24[1 + hal.global_hour_offset].weathernum].data) || (minute_shift < 10))
        {
            drawWeatherIcon(0, weather.hour24[hal.global_hour_offset].weathernum);
            i = 1;
        }
        for (; i < SAMPLE_COUNT; ++i)
        {
            if (weather_icons_day[weather.hour24[i + hal.global_hour_offset].weathernum].data != last_symb)
            {
                last_symb = weather_icons_day[weather.hour24[i + hal.global_hour_offset].weathernum].data;
                drawWeatherIcon(i * PX_PER_SAMPLE - minute_shift, weather.hour24[i + hal.global_hour_offset].weathernum);
            }
        }
    }

    void drawTime(int hour, int minute)
    {

        display.setFont(&FreeSans12pt7b);

        display.setTextColor(GxEPD_BLACK);
        display.setCursor(4, 18);
        display.fillRect(0, 0, 60, 18, GxEPD_WHITE);
        display.printf("%02d:%02d", hour, minute);

        display.setFont(&Picopixel);
        display.setCursor(71, 18);
        display.print(weather.LastUpdateTime);
        display.setCursor(71 + 16, 18);
        display.print("up");

        display.drawFastHLine(4, 20, 90, GxEPD_BLACK);
    }

    void drawDateAndDesc(int month, int date, int day)
    {
        int w1;
        u8g2Fonts.setCursor(5, 32);
        u8g2Fonts.printf("%02d月%02d日 星期%s", month, date, dayOfWeek[day]);

        display.drawFastHLine(4, 34, 90, GxEPD_BLACK);

        w1 = u8g2Fonts.getUTF8Width(weather.GDdistrict);
        u8g2Fonts.setCursor(48 - w1 / 2, 46);
        u8g2Fonts.print(weather.GDdistrict);

        u8g2Fonts.setCursor(2, 44 + 14);
        GUI::autoIndentDraw(weather.desc1, 88);
        u8g2Fonts.setCursor(100, 12);
        u8g2Fonts.print(weather.desc2);
    }
    void drawTemp(int max, int min, int x)
    {
        int w;
        char s[12];
        sprintf(s, "%d/%d", max, min);
        w = u8g2Fonts.getUTF8Width(s);
        u8g2Fonts.setCursor(96 + 35 + x - w, 77);
        u8g2Fonts.print(s);
    }
    void drawFrame()
    {

        display.fillRect(0, 96, 85, 200, GxEPD_WHITE);
        display.drawXBitmap(96, 2, weather_frames[0].data, weather_frames[0].width, weather_frames[0].height, GxEPD_BLACK);
        display.drawXBitmap(96 + 50, 2, weather_frames[1].data, weather_frames[1].width, weather_frames[1].height, GxEPD_BLACK);
        display.drawXBitmap(96 + 100, 2, weather_frames[2].data, weather_frames[2].width, weather_frames[2].height, GxEPD_BLACK);
        display.drawXBitmap(96 + 150, 2, weather_frames[3].data, weather_frames[3].width, weather_frames[3].height, GxEPD_BLACK);
        display.setFont(&FreeSans9pt7b);
        display.setCursor(96 + 12, 54);
        display.print(weather.realtime.temperature / 10);
        if (weather.realtime.humidity == 100)
            display.setCursor(96 + 8, 75);
        else if (weather.realtime.humidity == 0)
            display.setCursor(96 + 16, 75);
        else
            display.setCursor(96 + 12, 75);
        display.print(weather.realtime.humidity);
        drawWeatherIcon(96 + 50 + 12, weather.five_days[0].weathernum, 35);
        drawWeatherIcon(96 + 100 + 12, weather.five_days[1].weathernum, 35);
        drawWeatherIcon(96 + 150 + 12, weather.five_days[2].weathernum, 35);
        drawTemp(weather.five_days[0].max / 10, weather.five_days[0].min / 10, 50);
        drawTemp(weather.five_days[1].max / 10, weather.five_days[1].min / 10, 100);
        drawTemp(weather.five_days[2].max / 10, weather.five_days[2].min / 10, 150);
    }

    void drawLayout()
    {
        display.fillScreen(GxEPD_WHITE);
        hal.getTime();
        drawFrame();
        drawDateAndDesc(hal.timeinfo.tm_mon + 1, hal.timeinfo.tm_mday, hal.timeinfo.tm_wday);
        drawPlot(hal.timeinfo.tm_hour, hal.timeinfo.tm_min);
        drawTime(hal.timeinfo.tm_hour, hal.timeinfo.tm_min);
    }
};
static AppClock app;
static RTC_DATA_ATTR uint8_t NTPCounter = 0;
void appclock_wakeup()
{
    app.setup();
}
void AppClock::setup()
{
    exit = appclock_exit;
    deepsleep = appclock_deepsleep;
    wakeup = appclock_wakeup;
    int ntp_interval = hal.getNTPMinute();
    if (hal.wakeUpFromDeepSleep)
    {
        ++NTPCounter;
        if (NTPCounter < ntp_interval)
        {
            if (force_full_update == false && part_refresh_count < 15)
            {

                drawLayout();
                display.display(true);
                ++part_refresh_count;
                appManager.noDeepSleep = false;
                appManager.nextWakeup = 61 - hal.timeinfo.tm_sec;
                Serial.println("Finished part");
                return;
            }
        }
    }

    force_full_update = false;
    part_refresh_count = 0;
    display.setFullWindow();
    display.fillScreen(GxEPD_WHITE);
    hal.getTime();
    if (hal.timeinfo.tm_year < (2016 - 1900) || NTPCounter >= ntp_interval)
    {
        NTPCounter = 0;
        delay(20);
        hal.autoConnectWiFi();
        NTPSync();
        hal.getTime();
    }
    if (hal.now < weather.lastupdate || hal.now - weather.lastupdate > 60 * atoi(config[PARAM_FULLUPDATE].as<const char *>()))
    {
        hal.autoConnectWiFi();
        weather.refresh();
    }
    if (weather.hasAlert && weather.alertPubTime != last_alertPubTime)
    {
        last_alertPubTime = weather.alertPubTime;
        appManager.gotoApp("warning");
        force_full_update = true;
        return;
    }
    drawLayout();
    display.display(false);
    appManager.noDeepSleep = false;
    appManager.nextWakeup = 61 - hal.timeinfo.tm_sec;
    Serial.println("Finished full");
    return;
}
