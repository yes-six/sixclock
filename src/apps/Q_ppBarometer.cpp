#include "AppManager.h"

static const uint8_t BMP_icon[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0xfc, 0x3f, 0x00, 0x00, 0x0f, 0xf0, 0x00, 0x80, 0x03, 0xc0, 0x01,
    0xc0, 0x00, 0x00, 0x03, 0x60, 0x80, 0x01, 0x06, 0x30, 0x80, 0x01, 0x0c, 0x18, 0x06, 0x30, 0x18,
    0x08, 0x06, 0x30, 0x18, 0x0c, 0x00, 0x00, 0x30, 0x04, 0x00, 0x00, 0x20, 0xc6, 0x00, 0x80, 0x61,
    0xc6, 0x0c, 0x80, 0x61, 0x02, 0x18, 0x00, 0x40, 0x02, 0x30, 0x00, 0x40, 0x62, 0x60, 0x00, 0x43,
    0x62, 0x60, 0x00, 0x43, 0x02, 0xc0, 0x01, 0x40, 0x02, 0x80, 0x01, 0x40, 0x06, 0x00, 0x00, 0x60,
    0x06, 0x00, 0x00, 0x60, 0x04, 0x00, 0x00, 0x20, 0x0c, 0x70, 0x04, 0x30, 0x08, 0x90, 0x0e, 0x10,
    0x18, 0x90, 0x0a, 0x18, 0x30, 0x70, 0x0a, 0x0c, 0x60, 0x10, 0x11, 0x06, 0xc0, 0x10, 0x1f, 0x03,
    0x80, 0x11, 0x91, 0x01, 0x00, 0x07, 0xe0, 0x00, 0x00, 0xfc, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x00};

class AppBarometer : public AppBase
{
private:
public:
    AppBarometer()
    {
        name = "Barometer";
        title = "气压";
        description = "bmp气压计";
        image = BMP_icon;
        wakeupIO[1] = PIN_BUTTONR;
        peripherals_requested = PERIPHERALS_AHT20_BIT + PERIPHERALS_BMP280_BIT;
        _showInList = true;
    }
    void setup();
};
static AppBarometer app;
extern const char *dayOfWeek[];
void AppBarometer::setup()
{
    int32_t Refresh_Time = 10;
    display.clearScreen();
    u8g2Fonts.setFontMode(1);
    u8g2Fonts.setForegroundColor(0);
    u8g2Fonts.setBackgroundColor(1);

    if (hal.btnr.isPressing())
    {
        Serial.println("右键按下");
        if (GUI::waitLongPress(PIN_BUTTONR))
        {
            Serial.println("长按右键");

            const menu_item items[] = {
                {NULL, "返回"},
                {NULL, "设置刷新间隔"},
                {NULL, NULL},
            };

            int ret = GUI::menu("气压计设置", items);

            switch (ret)
            {
            case 0:
                break;
            case 1:
                Refresh_Time = GUI::msgbox_number("请输入刷新间隔s", 3, hal.pref.getInt("bmpup", 10));
                if (Refresh_Time < 5)
                {
                    Refresh_Time = 5;
                }
                else if (Refresh_Time > 100)
                {
                    Refresh_Time = 100;
                }
                hal.pref.putInt("bmpup", Refresh_Time);
                Serial.println(Refresh_Time);
                break;
            default:
                break;
            }

            display.display(true);
        }
    }

    Refresh_Time = hal.pref.getInt("bmpup", 10);
    u8g2Fonts.setCursor(5, 15);
    u8g2Fonts.printf("update %03d s", Refresh_Time);

    u8g2Fonts.setFont(u8g2_font_wqy12_t_gb2312);

    peripherals.bmp.takeForcedMeasurement();

    GUI::drawWindowsWithTitle("气压计", (296 - 160) / 2, (128 - 96) / 2, 160, 96);
    u8g2Fonts.setCursor(71, 16 + 30);
    u8g2Fonts.printf("当前气压：%.1fPa", peripherals.bmp.readPressure());
    u8g2Fonts.setCursor(71, 16 + 30 + 15);
    u8g2Fonts.printf("估算海拔：%.2fm", peripherals.bmp.readAltitude(weather.realtime.pressure / 100));

    display.drawFastHLine(0, 108, 296, 0);
    u8g2Fonts.setCursor(5, 125);
    u8g2Fonts.printf("%02d月%02d日 星期%s", hal.timeinfo.tm_mon + 1, hal.timeinfo.tm_mday, dayOfWeek[hal.timeinfo.tm_wday]);

    if (peripherals.peripherals_current & PERIPHERALS_AHT20_BIT)
    {
        sensors_event_t humidity, temp;
        peripherals.load_append(PERIPHERALS_AHT20_BIT);
        xSemaphoreTake(peripherals.i2cMutex, portMAX_DELAY);
        peripherals.aht.getEvent(&humidity, &temp);
        xSemaphoreGive(peripherals.i2cMutex);
        u8g2Fonts.printf("Temp:%.1f℃ Humi:%.1f%%", temp.temperature, humidity.relative_humidity);
    }

    display.drawXBitmap(296 - 25 - 9, 115, getBatteryIcon(), 8, 12, 0);
    u8g2Fonts.setCursor(296 - 25, 125);
    if (hal.USBPluggedIn)
    {
        display.drawXBitmap(296 - 25 + 2, 116, getUSBIcon(), 22, 10, 0);
    }
    else
    {
        u8g2Fonts.printf("%d%%", getBatteryNum());
    }

    if (force_full_update || part_refresh_count > 20)
    {
        display.display(false);
        force_full_update = false;
        part_refresh_count = 0;
    }
    else
    {
        display.display(true);
        part_refresh_count++;
    }
    appManager.noDeepSleep = false;
    appManager.nextWakeup = 61 - hal.timeinfo.tm_sec;
}
