#include "AppManager.h"

static const uint8_t Countdown_icon[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x00, 0x00, 0x00, 0x7c, 0x00,
    0x00, 0x00, 0xfe, 0x00, 0x00, 0x30, 0xce, 0x00, 0x00, 0x7c, 0xc6, 0x00, 0x00, 0x3e, 0x06, 0x00,
    0x00, 0x0f, 0x06, 0x00, 0x80, 0x03, 0x06, 0x00, 0xc0, 0x81, 0x7f, 0x00, 0xe0, 0xc0, 0xff, 0x00,
    0xe0, 0xe0, 0xcc, 0x01, 0x70, 0xf0, 0x8c, 0x03, 0x70, 0x70, 0x1c, 0x07, 0x70, 0x38, 0x1c, 0x0e,
    0x70, 0x18, 0x18, 0x0c, 0x70, 0x18, 0x18, 0x0c, 0xfe, 0x7f, 0x1c, 0x0c, 0x03, 0xc0, 0x1e, 0x0c,
    0x01, 0x80, 0x0f, 0x0c, 0x01, 0x80, 0x07, 0x0c, 0x99, 0x99, 0x00, 0x0e, 0x99, 0x99, 0x00, 0x07,
    0x01, 0x80, 0x80, 0x03, 0x01, 0x80, 0xc0, 0x01, 0x01, 0x80, 0xf0, 0x00, 0x03, 0xc0, 0x7f, 0x00,
    0xee, 0xff, 0x1f, 0x00, 0x38, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

class AppPopularComments : public AppBase
{
private:
public:
    AppPopularComments()
    {
        name = "PopularComments";
        title = "网易云热评";
        description = "网易云的热门评论";
        image = Countdown_icon;

        _showInList = false;
    }
    void setup();
};
static AppPopularComments app;

void AppPopularComments::setup()
{

    GUI::msgbox("网易云热评", "这个功能还没写好\n请按确定键返回");
}
