#include <A_Config.h>
#include "AppManager.h"

static const uint8_t Photobutton_icon[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0xf8, 0x1f, 0x00, 0x00, 0xfe, 0x7f, 0x00, 0x00, 0x0e, 0x70, 0x00, 0xf8, 0x07, 0xe0, 0x1f,
    0xfc, 0x03, 0xc0, 0x3f, 0x0e, 0x00, 0x00, 0x70, 0x06, 0x00, 0x00, 0x60, 0x06, 0x00, 0x00, 0x63,
    0x06, 0xf0, 0x0f, 0x63, 0x06, 0xf8, 0x1f, 0x60, 0x06, 0x1c, 0x38, 0x60, 0x06, 0x0c, 0x30, 0x60,
    0x06, 0x0c, 0x30, 0x60, 0x06, 0x0c, 0x30, 0x60, 0x06, 0x0c, 0x30, 0x60, 0x06, 0x0c, 0x30, 0x60,
    0x06, 0x1c, 0x38, 0x60, 0x06, 0xf8, 0x1f, 0x60, 0x06, 0xf0, 0x0f, 0x60, 0x06, 0x00, 0x00, 0x60,
    0x06, 0x00, 0x00, 0x60, 0x0e, 0x00, 0x00, 0x70, 0xfc, 0xff, 0xff, 0x3f, 0xf8, 0xff, 0xff, 0x1f,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

class AppPhotobutton : public AppBase
{
private:
public:
  AppPhotobutton()
  {
    name = "Photobutton";
    title = "远程拍照";
    description = "类似自拍杆";
    image = Photobutton_icon;

    _showInList = true;
  }
  void setup();
  void update();
};
static AppPhotobutton app;
extern const char *dayOfWeek[];
bool Pbbuz_Mode = 0; // 0无声音,1有声音

void AppPhotobutton::setup()
{
  int Photocount = 0;
  Pbbuz_Mode = hal.pref.getBool(SETTINGS_PARAM_PhotoBtn_buz_Mode, 0);

  const menu_item items[] = {
      {NULL, "返回"},
      {NULL, "拍照提示音设置"},
      {NULL, "退出"},
      {NULL, NULL},
  };

  display.clearScreen();
  u8g2Fonts.setFontMode(1);
  u8g2Fonts.setForegroundColor(0);
  u8g2Fonts.setBackgroundColor(1);

  bleKey.begin();
  GUI::msgbox("蓝牙遥控拍照", "请使用手机蓝牙连接\n“SixClock”\n若无法连接,请删除配对信息后\n再次尝试连接");
  GUI::drawWindowsWithTitle("蓝牙遥控拍照", (296 - 160) / 2, (128 - 96) / 2, 160, 96);
  u8g2Fonts.setCursor(71, 16 + 30);
  u8g2Fonts.print("请打开手机相机");
  u8g2Fonts.setCursor(71, 16 + 30 + 15);
  u8g2Fonts.print("拨动设备右键进行遥控拍照!");
  display.display(true);
  display.clearScreen();
  do
  {
    if (hal.btnr.isPressing())
    {
      while (hal.btnr.isPressing())
      {
        delay(10);
      }
      if (bleKey.isConnected())
      {
        Serial.println("Sending Play/Pause media key...");
        bleKey.write(KEY_MEDIA_VOLUME_DOWN);
        Photocount++;
        Serial.println(Photocount);
        display.clearScreen();
        u8g2Fonts.setCursor(5, 15);
        u8g2Fonts.printf("本次已拍照 %03d 张", Photocount);
        update();
      }
      else
      {
        Serial.println("not connected");
        GUI::msgbox("蓝牙遥控拍照", "拍照失败\n未连接手机\n请先连接SixClock");
        display.display(true);
      }
    }
    else if (hal.btnl.isPressing())
    {
      while (hal.btnl.isPressing())
      {
        delay(10);
      }
      if (bleKey.isConnected())
      {
        Serial.println("Sending Play/Pause media key...");
        bleKey.write(KEY_MEDIA_VOLUME_UP);
        Photocount++;
        Serial.println(Photocount);
        display.clearScreen();
        u8g2Fonts.setCursor(5, 15);
        u8g2Fonts.printf("本次已拍照 %03d 张", Photocount);
        update();
      }
      else
      {
        Serial.println("not connected");
        GUI::msgbox("蓝牙遥控拍照", "拍照失败\n未连接手机\n请先连接SixClock");
        display.display(true);
      }
    }
    else if (hal.btnc.isPressing())
    {
      if (GUI::waitLongPress(PIN_BUTTONC))
      {
        delay(100);
        Serial.println("长按中键");

        int ret = GUI::menu("拍照设置", items);
        switch (ret)
        {
        case 0:
        {
        }
        break;
        case 1:
        {
          if (GUI::msgbox_yn("拍照提示音设置", "请选择", "开启(右)", "关闭(左)"))
          {
            Pbbuz_Mode = 1;
            hal.pref.putBool(SETTINGS_PARAM_PhotoBtn_buz_Mode, 1);
          }
          else
          {
            Pbbuz_Mode = 0;
            hal.pref.putBool(SETTINGS_PARAM_PhotoBtn_buz_Mode, 0);
          }
        }
        break;
        case 2:
        {
          bleKey.end();
          appManager.goBack();
          return;
        }
        break;
        default:
          break;
        }
      }
      display.display(true);
    }
  } while (1);
}

void AppPhotobutton::update()
{
  if (Pbbuz_Mode)
  {
    ledcSetup(0, 3000, 10);
    ledcAttachPin(PIN_BUZZER, 0);
    ledcWriteTone(0, 3000);
    ledcWrite(0, 20);
    delay(100);
    ledcWrite(0, 0);
    delay(50);
    ledcWrite(0, 20);
    delay(100);
    ledcWriteTone(0, 0);
    ledcDetachPin(PIN_BUZZER);
  }

  u8g2Fonts.setFont(u8g2_font_wqy12_t_gb2312);
  u8g2Fonts.setCursor((296 - (u8g2Fonts.getUTF8Width("按下左/右键进行拍照!"))) / 2, 88);
  u8g2Fonts.print("按下左/右键进行拍照!");
  display.drawXBitmap((296 - 32) / 2, (108 - 32) / 2, Photobutton_icon, 32, 32, 0);
  display.drawFastHLine(0, 108, 296, 0);
  u8g2Fonts.setCursor(5, 125);
  u8g2Fonts.printf("%02d月%02d日 星期%s %02d:%02d ", hal.timeinfo.tm_mon + 1, hal.timeinfo.tm_mday, dayOfWeek[hal.timeinfo.tm_wday], hal.timeinfo.tm_hour, hal.timeinfo.tm_min);

  if (peripherals.peripherals_current & PERIPHERALS_AHT20_BIT)
  {
    sensors_event_t humidity, temp;
    peripherals.load_append(PERIPHERALS_AHT20_BIT);
    xSemaphoreTake(peripherals.i2cMutex, portMAX_DELAY);
    peripherals.aht.getEvent(&humidity, &temp);
    xSemaphoreGive(peripherals.i2cMutex);
    u8g2Fonts.printf("Temp:%.1f℃ Humi:%.1f%%", temp.temperature, humidity.relative_humidity);
  }

  display.drawXBitmap(296 - 25 - 9, 115, getBatteryIcon(), 8, 12, 0);
  u8g2Fonts.setCursor(296 - 25, 125);
  if (hal.USBPluggedIn)
  {
    display.drawXBitmap(296 - 25 + 2, 116, getUSBIcon(), 22, 10, 0);
  }
  else
  {
    u8g2Fonts.printf("%d%%", getBatteryNum());
  }

  if (force_full_update || part_refresh_count > 20)
  {
    display.display(false);
    force_full_update = false;
    part_refresh_count = 0;
  }
  else
  {
    display.display(true);
    part_refresh_count++;
  }
}
