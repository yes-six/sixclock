#include "hal.h"
#include <LittleFS.h>
#include "img_hal.h"
#include "img_esptouch.h"

void task_hal_update(void *)
{
    while (1)
    {
        if (hal._hookButton)
        {
            while (hal.btnr.isPressing() || hal.btnl.isPressing() || hal.btnc.isPressing())
            {
                hal.btnr.tick();
                hal.btnl.tick();
                hal.btnc.tick();
                delay(20);
            }
            hal.btnr.tick();
            hal.btnl.tick();
            hal.btnc.tick();
            while (hal._hookButton)
            {
                while (hal.SleepUpdateMutex)
                    delay(10);
                hal.update();
                delay(20);
            }
            while (hal.btnr.isPressing() || hal.btnl.isPressing() || hal.btnc.isPressing())
            {
                delay(20);
            }
        }
        while (hal.SleepUpdateMutex)
            delay(10);
        hal.SleepUpdateMutex = true;
        hal.btnr.tick();
        hal.btnl.tick();
        hal.btnc.tick();
        hal.SleepUpdateMutex = false;
        delay(20);
        while (hal.SleepUpdateMutex)
            delay(10);
        hal.SleepUpdateMutex = true;
        hal.btnr.tick();
        hal.btnl.tick();
        hal.btnc.tick();
        hal.update();
        hal.SleepUpdateMutex = false;
        delay(20);
    }
}
void HAL::saveConfig()
{
    File configFile = LittleFS.open("/config.json", "w");
    if (!configFile)
    {
        Serial.println("无法打开用于写入的配置文件");
        return;
    }
    serializeJson(config, configFile);
    configFile.close();
}
void HAL::loadConfig()
{
    File configFile = LittleFS.open("/config.json", "r");
    if (!configFile)
    {
        Serial.println("无法打开配置文件");
        return;
    }
    deserializeJson(config, configFile);
    configFile.close();
}

void HAL::getTime()
{
    int64_t tmp;
    if (peripherals.peripherals_current & PERIPHERALS_DS3231_BIT)
    {
        xSemaphoreTake(peripherals.i2cMutex, portMAX_DELAY);
        timeinfo.tm_year = peripherals.rtc.getYear() + 2000;
        timeinfo.tm_mon = peripherals.rtc.getMonth();
        timeinfo.tm_mday = peripherals.rtc.getDate();
        timeinfo.tm_hour = peripherals.rtc.getHour();
        timeinfo.tm_min = peripherals.rtc.getMinute();
        timeinfo.tm_sec = peripherals.rtc.getSecond();
        timeinfo.tm_wday = peripherals.rtc.getDoW() - 1;
        now = mktime(&timeinfo);
        xSemaphoreGive(peripherals.i2cMutex);
    }
    else
    {
        time(&now);
        if (delta != 0 && lastsync < now)
        {

            tmp = now - lastsync;
            tmp *= delta;
            tmp /= every;
            now -= tmp;
        }
        localtime_r(&now, &timeinfo);
    }
}

void HAL::WiFiConfigSmartConfig()
{
    display.fillScreen(GxEPD_WHITE);
    display.drawXBitmap(0, 0, smartconfig, 296, 128, GxEPD_BLACK);
    display.display();
    WiFi.beginSmartConfig();
    Serial.println("开始EspTouch SmartConfig配网");
    int count = 0;
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
        Serial.println(WiFi.status());
        count++;
        if (count >= 240)
        {
            Serial.println("SmartConfig超时");
            display.fillScreen(GxEPD_WHITE);

            GUI::drawWindowsWithTitle("错误", (296 - 160) / 2, (128 - 96) / 2, 160, 96);
            u8g2Fonts.setCursor(71, 16 + 30);
            u8g2Fonts.print("SmartConfig配置超时");
            u8g2Fonts.setCursor(71, 16 + 30 + 15);
            u8g2Fonts.print("请拨动左键使设备重启");
            display.display();

            hal.powerOff(0);
            Serial.println("正在重启");
            ESP.restart();
        }

        if (hal.btnl.isPressing())
        {
            while (hal.btnl.isPressing())
                delay(20);
            hal.powerOff(0);
            ESP.restart();
            break;
        }
    }

    if (WiFi.waitForConnectResult() == WL_CONNECTED)
    {
        Serial.println("WiFi connected");
        config[PARAM_SSID] = WiFi.SSID();
        config[PARAM_PASS] = WiFi.psk();
        hal.saveConfig();
    }
}

#include <DNSServer.h>
void HAL::WiFiConfigManual()
{
    DNSServer dnsServer;
#include "img_manual.h"
    String passwd = String((esp_random() % 1000000000L) + 10000000L);
    String str = "WIFI:T:WPA2;S:Six-Clock;P:" + passwd + ";;";
    WiFi.softAP("Six-Clock", passwd.c_str());
    WiFi.softAPConfig(IPAddress(192, 168, 4, 1), IPAddress(192, 168, 4, 1), IPAddress(255, 255, 255, 0));
    dnsServer.start(53, "*", IPAddress(192, 168, 4, 1));
    beginWebServer();
    display.fillScreen(GxEPD_WHITE);

    GUI::drawWindowsWithTitle("SoftAP", 0, 0, 296, 128);
    QRCode qrcode;
    uint8_t qrcodeData[qrcode_getBufferSize(7)];
    qrcode_initText(&qrcode, qrcodeData, 6, 0, str.c_str());
    Serial.println(qrcode.size);
    for (uint8_t y = 0; y < qrcode.size; y++)
    {

        for (uint8_t x = 0; x < qrcode.size; x++)
        {
            display.fillRect(2 * x + 20, 2 * y + 30, 2, 2, qrcode_getModule(&qrcode, x, y) ? GxEPD_BLACK : GxEPD_WHITE);
        }
    }

    u8g2Fonts.setCursor(140, 40);
    u8g2Fonts.print("已经启动SoftAP");
    u8g2Fonts.setCursor(140, 55);
    u8g2Fonts.print("请扫描左侧二维码进行连接");
    u8g2Fonts.setCursor(140, 70);
    u8g2Fonts.print("或输入密码进行连接");
    u8g2Fonts.setCursor(140, 85);
    u8g2Fonts.print("拨动左键可重启设备");

    u8g2Fonts.setCursor(140, 97);
    u8g2Fonts.print("WIFI: Six-Clock");
    u8g2Fonts.setCursor(140, 111);
    u8g2Fonts.print("密码:");
    display.setFont(&FreeSans9pt7b);
    display.setCursor(170, 112);
    display.print(passwd);
    display.display();
    uint32_t last_millis = millis();
    while (1)
    {
        dnsServer.processNextRequest();
        updateWebServer();
        delay(5);
        if (WiFi.softAPgetStationNum() == 0)
        {
            last_millis = millis();
        }
        if (millis() - last_millis > 600000)
        {
            Serial.println("手动配置超时");
            display.fillScreen(GxEPD_WHITE);
            u8g2Fonts.setCursor(71, 80);
            u8g2Fonts.print("手动配置超时");
            display.display();
            delay(100);
            hal.powerOff(false);
            ESP.restart();
        }
        if (LuaRunning)
            continue;
        if (hal.btnl.isPressing())
        {
            while (hal.btnl.isPressing())
                delay(20);

            ESP.restart();
            break;
        }
    }
}
void HAL::ReqWiFiConfig()
{
    display.fillScreen(GxEPD_WHITE);
    GUI::drawWindowsWithTitle("Wifi连接异常", 0, 0, 296, 128);
    display.display();
    u8g2Fonts.setCursor(5, 30);
    u8g2Fonts.print("无法连接到WiFi");
    u8g2Fonts.setCursor(5, 45);
    u8g2Fonts.print("向左:使用SoftAP配网");
    u8g2Fonts.setCursor(5, 60);
    u8g2Fonts.print("向右:使用智能配网");
    u8g2Fonts.setCursor(5, 75);
    u8g2Fonts.print("中间:使用离线模式");
    display.display();
    uint32_t last_millis = millis();
    while (1)
    {
        if (hal.btnl.isPressing())
        {
            WiFiConfigManual();
            break;
        }
        if (hal.btnr.isPressing())
        {
            WiFiConfigSmartConfig();
            break;
        }
        if (hal.btnc.isPressing())
        {
            WiFi.disconnect(true);
            config[PARAM_CLOCKONLY] = "1";
            hal.saveConfig();
            ESP.restart();
            break;
        }
        delay(5);
        if (millis() - last_millis > 60000)
        {
            Serial.println("WiFi配置方式选择超时");
            break;
        }
    }
    if (WiFi.isConnected() == false)
    {
        config[PARAM_CLOCKONLY] = "1";
        hal.saveConfig();
        ESP.restart();
    }
}

void test_littlefs_size(bool format = true)
{
    uint32_t size_request;
    size_t size_physical = 0;
    esp_flash_get_physical_size(esp_flash_default_chip, &size_physical);
    size_request = size_physical - 0x300000 - 0x1000;
    if (hal.pref.getUInt("size", 0) != size_request)
    {
        Serial.println("检测到分区大小不一致，正在格式化");
        hal.pref.putUInt("size", size_request);
        LittleFS.format();
    }
}

bool HAL::init()
{
    int16_t total_gnd = 0;
    bool timeerr = false;
    bool initial = true;
    Serial.begin(115200);
    setenv("TZ", "CST-8", 1);
    tzset();

    pref.begin("clock");
    pinMode(PIN_BUTTONR, INPUT);
    pinMode(PIN_BUTTONL, INPUT);
    pinMode(PIN_BUTTONC, INPUT);
    total_gnd += digitalRead(PIN_BUTTONR);
    total_gnd += digitalRead(PIN_BUTTONL);
    total_gnd += digitalRead(PIN_BUTTONC);
    if (total_gnd <= 1)
    {
        btnl._buttonPressed = 1;
        btnr._buttonPressed = 1;
        btnc._buttonPressed = 1;
        btn_activelow = false;
    }
    else
    {
        ESP_LOGW("HAL", "此设备为旧版硬件，建议尽快升级以获得最佳体验。");
        btnl._buttonPressed = 0;
        btnr._buttonPressed = 0;
        btnc._buttonPressed = 0;
        btn_activelow = true;
    }
    esp_task_wdt_init(portMAX_DELAY, false);
    pinMode(PIN_CHARGING, INPUT);
    pinMode(PIN_SD_CARDDETECT, INPUT_PULLUP);
    pinMode(PIN_SDVDD_CTRL, OUTPUT);
    digitalWrite(PIN_SDVDD_CTRL, 1);

    pinMode(PIN_BUZZER, OUTPUT);
    digitalWrite(PIN_BUZZER, 0);

    if (pref.getUInt("lastsync") == 0)
    {
        pref.putUInt("lastsync", 1);
        pref.putInt("every", 100);
        pref.putInt("delta", 0);
        pref.putInt("upint", 2 * 60);
    }
    lastsync = pref.getUInt("lastsync", 1);
    every = pref.getInt("every", 100);
    delta = pref.getInt("delta", 0);
    upint = pref.getInt("upint", 2 * 60);

    getTime();
    if ((timeinfo.tm_year < (2016 - 1900)))
    {
        timeerr = true;
        pref.putUInt("lastsync", 1);
        lastsync = 1;
    }
    if (esp_sleep_get_wakeup_cause() != ESP_SLEEP_WAKEUP_UNDEFINED)
        initial = false;

    WiFi.mode(WIFI_OFF);

    display.init(0, initial);
    display.setRotation(pref.getUChar(SETTINGS_PARAM_SCREEN_ORIENTATION, 3));
    display.setTextColor(GxEPD_BLACK);
    u8g2Fonts.setFontMode(1);
    u8g2Fonts.setForegroundColor(GxEPD_BLACK);
    u8g2Fonts.setBackgroundColor(GxEPD_WHITE);
    u8g2Fonts.setFont(u8g2_font_wqy12_t_gb2312);
    u8g2Fonts.begin(display);
    if (hal.btnl.isPressing() && (esp_sleep_get_wakeup_cause() == ESP_SLEEP_WAKEUP_UNDEFINED))
    {

        powerOff(true);
        ESP.restart();
    }
    if (LittleFS.begin(false) == false)
    {
        display.fillScreen(GxEPD_WHITE);
        u8g2Fonts.setCursor(71, 80);
        u8g2Fonts.print("等待LittleFS格式化");
        display.display();
        LittleFS.format();
        if (LittleFS.begin(false) == false)
        {
            Serial.println("LittleFS格式化失败");
            display.fillScreen(GxEPD_WHITE);
            u8g2Fonts.setCursor(71, 80);
            u8g2Fonts.print("LittleFS格式化失败");
            display.display(true);
            delay(100);
            powerOff(false);
            ESP.restart();
        }
        test_littlefs_size(false);
    }
    test_littlefs_size(true);
    if (LittleFS.exists("/config.json") == false)
    {
        Serial.println("正在写入默认配置");
        File f = LittleFS.open("/config.json", "w");
        f.print(DEFAULT_CONFIG);
        f.close();
    }
    loadConfig();
    weather.begin();

    peripherals.init();
    buzzer.init();
    xTaskCreate(task_hal_update, "hal_update", 2048, NULL, 10, NULL);
    if (initial == false && timeerr == false)
    {
        return false;
    }
    if (((analogRead(PIN_ADC) * atoi(config[PARAM_BATV].as<const char *>())) / 4095) < 3000)
    {
        powerOff(2);
        ESP.restart();
    }
    return true;
}

void HAL::autoConnectWiFi()
{
    if (WiFi.isConnected())
    {
        return;
    }

    if (config[PARAM_SSID] == "")
    {
        ReqWiFiConfig();
    }
    else
    {
        WiFi.setHostname("weatherclock");
        WiFi.mode(WIFI_STA);
        WiFi.begin(config[PARAM_SSID].as<const char *>(), config[PARAM_PASS].as<const char *>());
    }
    if (!WiFi.isConnected())
    {
        if (WiFi.waitForConnectResult(20000) != WL_CONNECTED)
        {
            hal.ReqWiFiConfig();
        }
    }
    sntp_stop();
    Serial.println("Wifi已连接");
}
static void set_sleep_set_gpio_interrupt()
{
    if (hal.btn_activelow)
    {
        esp_sleep_enable_ext0_wakeup((gpio_num_t)hal._wakeupIO[0], 0);
        esp_sleep_enable_ext1_wakeup((1LL << hal._wakeupIO[1]), ESP_EXT1_WAKEUP_ALL_LOW);
    }
    else
    {
        esp_sleep_enable_ext1_wakeup((1ULL << PIN_BUTTONC) | (1ULL << PIN_BUTTONL) | (1ULL << PIN_BUTTONR), ESP_EXT1_WAKEUP_ANY_HIGH);
    }
}

#include "driver/ledc.h"
static void pre_sleep()
{
    peripherals.sleep();
    set_sleep_set_gpio_interrupt();
    display.hibernate();
    buzzer.waitForSleep();
    delay(10);
    ledcDetachPin(PIN_BUZZER);
    digitalWrite(PIN_BUZZER, 0);
}

void HAL::goSleep(uint32_t sec)
{
    hal.getTime();
    long nextSleep = 0;
    if (sec != 0)
        nextSleep = sec;
    else
    {
        nextSleep = 1;
    }
    Serial.printf("下次唤醒:%ld s\n", nextSleep);
    nextSleep = nextSleep * 1000000UL;
    pre_sleep();
    esp_sleep_enable_timer_wakeup(nextSleep);

    delay(1);
    if (noDeepSleep)
    {
        esp_light_sleep_start();
        display.init(0, false);
        peripherals.wakeup();
        ledcAttachPin(PIN_BUZZER, 0);
    }
    else
    {
        esp_deep_sleep_start();
    }
}

void HAL::powerOff(uint8_t displayMessage)
{
    if (displayMessage == 0)
    {
    }
    else if (displayMessage == 1)
    {
        display.setFullWindow();
        display.fillScreen(GxEPD_WHITE);
        display.drawXBitmap(0, 0, poweroff, 296, 128, GxEPD_BLACK);
        display.display();
    }
    else if (displayMessage == 2)
    {
        int w;
        display.setFullWindow();
        display.fillScreen(GxEPD_WHITE);
        display.drawXBitmap(0, 0, poweroff, 296, 128, GxEPD_BLACK);
        Serial.println("显示关机界面完成");

        w = u8g2Fonts.getUTF8Width("电量不足,已关机,请充电");
        u8g2Fonts.setCursor((296 - w) / 2, 100);
        u8g2Fonts.print("电量不足,已关机,请充电");
        display.display();
    }
    else
    {
        display.setFullWindow();
        display.fillScreen(GxEPD_WHITE);
        display.drawXBitmap(0, 0, poweroff, 296, 128, GxEPD_BLACK);
        display.display();
    }
    force_full_update = true;
    pre_sleep();
    WiFi.disconnect(true);
    set_sleep_set_gpio_interrupt();

    delay(1);
    if (noDeepSleep)
    {
        esp_light_sleep_start();
        display.init(0, false);
        peripherals.wakeup();
    }
    else
    {
        esp_deep_sleep_start();
    }
}
void HAL::update(void)
{
    static int count = 0;
    if (count++ % 30 == 0)
    {
        count = 0;
        getTime();

        long adc;

        adc = analogRead(PIN_ADC);
        adc = (adc * atoi(config[PARAM_BATV].as<const char *>())) / 4095;

        VCC = adc;
        if (adc > 4300)
        {
            USBPluggedIn = true;
        }
        else
        {
            USBPluggedIn = false;
        }
        if (digitalRead(PIN_CHARGING) == 0)
        {
            isCharging = true;
        }
        else
        {
            isCharging = false;
        }
    }
}
int HAL::getNTPMinute()
{
    int res[] = {
        0,
        2 * 60,
        4 * 60,
        6 * 60,
        12 * 60,
        24 * 60,
        36 * 60,
        48 * 60,
    };
    int val = pref.getUChar(SETTINGS_PARAM_NTP_INTERVAL, 1);
    return res[val];
}
#include "img_goodnightmorning.h"
uint8_t RTC_DATA_ATTR night_sleep_today = -1;
uint8_t RTC_DATA_ATTR night_sleep = 0;
void HAL::checkNightSleep()
{
    if (hal.timeinfo.tm_year < (2016 - 1900))
    {
        Serial.println("[夜间模式] 时间错误，直接返回");
        return;
    }
    if (config[PARAM_SLEEPATNIGHT].as<String>() == "0")
    {
        Serial.println("[夜间模式] 夜间模式已禁用");
        return;
    }
    if (night_sleep_today == hal.timeinfo.tm_mday)
    {
        Serial.println("[夜间模式] 当天暂时退出夜间模式");
        return;
    }
    if (hal.timeinfo.tm_year < (2016 - 1900))
    {
        Serial.println("[夜间模式] 时间错误");
        night_sleep = 0;
        night_sleep_today = -1;
        return;
    }
    String tmp = config[PARAM_SLEEPATNIGHT_START].as<String>();

    int sleepStart = tmp.substring(0, 2).toInt() * 60 + tmp.substring(3, 5).toInt();
    tmp = config[PARAM_SLEEPATNIGHT_END].as<String>();
    int sleepEnd = tmp.substring(0, 2).toInt() * 60 + tmp.substring(3, 5).toInt();
    bool end_at_nextday = sleepStart > sleepEnd;
    int now = hal.timeinfo.tm_hour * 60 + hal.timeinfo.tm_min;
    uint8_t night_sleep_pend = 0;
    if (end_at_nextday)
    {
        if (now >= sleepStart)
        {

            night_sleep_pend = 1;
        }
        else if (now < sleepEnd)
        {

            night_sleep_pend = 2;
        }
        else
        {
            night_sleep_pend = 0;
        }
    }
    else
    {
        int mid = sleepStart + sleepEnd;
        mid = mid / 2;
        if (now >= sleepStart && now <= sleepEnd)
        {
            if (now < mid)
            {
                night_sleep_pend = 1;
            }
            else
            {
                night_sleep_pend = 2;
            }
        }
        else
        {
            night_sleep_pend = 0;
        }
    }

    if (night_sleep != night_sleep_pend)
    {
        Serial.println("[DEBUG] 夜间模式重绘");
        night_sleep = night_sleep_pend;
        display.clearScreen();
        if (night_sleep == 1)
        {

            display.drawXBitmap(0, 0, goodnight_bits, 296, 128, 0);
        }
        else if (night_sleep == 2)
        {

            display.drawXBitmap(0, 0, goodmorning_bits, 296, 128, 0);
        }
        display.display(false);
    }

    if (night_sleep != 0)
    {
        hal.goSleep(1800);
    }
}
void HAL::setWakeupIO(int io1, int io2)
{
    _wakeupIO[0] = io1;
    _wakeupIO[1] = io2;
}
void HAL::copy(File &newFile, File &file)
{
    char *buf = (char *)malloc(512);
    size_t currentsize = 0;
    if (!buf)
    {
        Serial.println("内存已满");
        ESP.restart();
    }
    while (1)
    {
        currentsize = file.readBytes(buf, 512);
        if (currentsize == 0)
            break;
        newFile.write((uint8_t *)buf, currentsize);
    }
    free(buf);
}
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>

void HAL::rm_rf(const char *path)
{
    DIR *dp;
    struct dirent *entry;
    struct stat statbuf;

    if ((dp = opendir(path)) == NULL)
    {
        perror("opendir");
        return;
    }

    while ((entry = readdir(dp)) != NULL)
    {

        char filePath[256];
        sprintf(filePath, "%s/%s", path, entry->d_name);

        if (stat(filePath, &statbuf) == -1)
        {
            perror("lstat");
            continue;
        }

        if (S_ISDIR(statbuf.st_mode))
        {

            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            {
                continue;
            }

            rm_rf(filePath);
        }
        else
        {

            if (remove(filePath) != 0)
            {
                perror("remove");
            }
        }
    }

    closedir(dp);

    if (rmdir(path) != 0)
    {
        perror("rmdir");
    }
}

void HAL::waitClick()
{
    while (hal.btnl.isPressing() == false && hal.btnr.isPressing() == false && hal.btnc.isPressing() == false)
        delay(10);
}

HAL hal;