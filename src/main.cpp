#include <A_Config.h>
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"

U8G2_FOR_ADAFRUIT_GFX u8g2Fonts;
GxEPD2_BW<GxEPD2_290, GxEPD2_290::HEIGHT> display(GxEPD2_290(/*CS=5*/ CONFIG_SPI_CS, CONFIG_PIN_DC, CONFIG_PIN_RST, CONFIG_PIN_BUSY));
DynamicJsonDocument config(1024);
BleKeyboard bleKey("SixClock", "Espressif", 95);

void task_appManager(void *)
{
    while (1)
    {
        appManager.update();
        delay(20);
    }
}
#include <LittleFS.h>
void setup()
{

    WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
    hal.init();
    Serial.println("完成hal.init()");
    alarms.load();
    alarms.check();
    Serial.print("当前CPU频率：");
    Serial.println(ESP.getCpuFreqMHz());

    xTaskCreate(task_appManager, "appManager", 20480, NULL, 1, NULL);
    if (hal.pref.getInt("oobe", 0) <= 2)
    {
        appManager.gotoApp("oobe");
        return;
    }
    hal.getTime();
    if (hal.timeinfo.tm_year > (2016 - 1900))
    {

        if (esp_sleep_get_wakeup_cause() == ESP_SLEEP_WAKEUP_EXT0 || esp_sleep_get_wakeup_cause() == ESP_SLEEP_WAKEUP_EXT1)
        {
            if (night_sleep != 0)
            {
                night_sleep = 0;
                night_sleep_today = hal.timeinfo.tm_mday;
            }
        }
        hal.checkNightSleep();
    }
    bool recoverLast = false;
    hal.wakeUpFromDeepSleep = false;
    if (esp_sleep_get_wakeup_cause() != ESP_SLEEP_WAKEUP_UNDEFINED)
    {
        hal.wakeUpFromDeepSleep = true;
        recoverLast = appManager.recover(appManager.getRealClock());
    }
    if (recoverLast == false)
    {
        appManager.gotoApp(appManager.getRealClock());
    }
    return;
}

int NTPCounter = 0;
void loop()
{
    vTaskDelete(NULL);
    vTaskDelay(portMAX_DELAY);
}
