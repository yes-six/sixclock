#pragma once

#define SETTINGS_PARAM_NTP_INTERVAL "p1"

#define SETTINGS_PARAM_PHERIPHERAL_BITMASK "p2"

#define SETTINGS_PARAM_SCREEN_ORIENTATION "p3"

/**
 * 电子书上次打开的文件
 */
#define SETTINGS_PARAM_LAST_EBOOK "p4"

/**
 * 电子书上次打开的页码
 */
#define SETTINGS_PARAM_LAST_EBOOK_PAGE "p5"

/**
 * 自定义主屏幕App
 */
#define SETTINGS_PARAM_HOME_APP "boot"

/**
 * 自定义闹钟铃声文件
 * 字符串，铃声是.buz文件
 * 如果是空则为“滴滴”声音
 */
#define SETTINGS_PARAM_ALARM_TONE "p6"

#define SETTINGS_PARAM_WordBookNum "wbnum"

#define SETTINGS_PARAM_WordBook_Random_Mode "wbran"

#define SETTINGS_PARAM_PhotoBtn_buz_Mode "pbbuz"
