#pragma once
#include <Arduino.h>
#include <Adafruit_AHTX0.h>
#include <Adafruit_BMP280.h>
#include <Adafruit_SGP30.h>
#include <Wire.h>
#include <SD.h>
#include <DS3231.h>
#define PERIPHERALS_SD_BIT 1
#define PERIPHERALS_AHT20_BIT 2
#define PERIPHERALS_BMP280_BIT 4
#define PERIPHERALS_SGP30_BIT 8
#define PERIPHERALS_DS3231_BIT 16

class Peripherals
{
private:
    bool ahtInited = false;
    bool bmpInited = false;
    bool sgpInited = false;

public:
    uint16_t peripherals_current = 0;
    uint16_t peripherals_load = 0;
    void check();
    void init();
    void initSGP();
    uint16_t checkAvailable(uint16_t bitmask);
    bool load(uint16_t bitmask);
    void sleep();
    void wakeup();
    bool isSDLoaded()
    {
        return (peripherals_load & PERIPHERALS_SD_BIT) == PERIPHERALS_SD_BIT;
    }
    void load_append(uint16_t bitmask);

    Adafruit_AHTX0 aht;
    Adafruit_BMP280 bmp;
    Adafruit_SGP30 sgp;
    DS3231 rtc;
    SemaphoreHandle_t i2cMutex = NULL;
};
extern Peripherals peripherals;
