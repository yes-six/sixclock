#pragma once

typedef struct
{
    const uint8_t *icon;
    const char *title;
} menu_item;

namespace GUI
{
    extern int last_buffer_idx;
    bool waitLongPress(int btn);
    void autoIndentDraw(const char *str, int max_x, int start_x = 2);
    void drawWindowsWithTitle(const char *title = NULL, int16_t x = 0, int16_t y = 0, int16_t w = 296, int16_t h = 128);
    void msgbox(const char *title, const char *msg);
    bool msgbox_yn(const char *title, const char *msg, const char *yes = NULL, const char *no = NULL);
    int msgbox_number(const char *title, uint16_t digits, int pre_value);
    int msgbox_time(const char *title, int pre_value);
    int menu(const char *title, const menu_item options[], int16_t ico_w = 8, int16_t ico_h = 8);
    void drawLBM(int16_t x, int16_t y, const char *filename, uint16_t color);

    const char *fileDialog(const char *title, bool isApp = false, const char *endsWidth = NULL);
}
