#pragma once
#include <A_Config.h>
#include <list>
#include <stack>

class AppBase
{
private:
public:
    const char *name = "BaseApp";
    const char *title = "BaseApp";
    const char *description = "BaseApp";
    const uint8_t *image = NULL;
    int appID = 0;
    bool _showInList = true;
    int wakeupIO[2] = {PIN_BUTTONC, PIN_BUTTONL};
    bool noDefaultEvent = false;
    uint16_t peripherals_requested = 0;
    bool isLuaApp = false;
    bool _reentrant = true;
    /**
     * @brief 初始化(App打开)
     */
    virtual void setup(){};
    /**
     * @brief 进入lightsleep前执行
     */
    void (*lightsleep)();
    /**
     * @brief lightsleep唤醒后执行
     */
    void (*wakeup)();
    /**
     * @brief App退出时执行
     */
    void (*exit)();
    /**
     * @brief 进入deepsleep前执行，注意如果没有此函数，会尝试执行exit函数
     */
    void (*deepsleep)();
    AppBase();
    ~AppBase();
};

class AppManager
{
private:
    std::stack<AppBase *> appStack;
    enum
    {
        APPMANAGER_NOOPERATION = 0,
        APPMANAGER_GOBACK = 1,
        APPMANAGER_GOTOAPP = 2,
        APPMANAGER_SHOWAPPSELECTOR = 3,
    } method;
    int validAppID = 1;
    void (*fTimer)() = NULL;
    uint32_t timer_interval = 0;
    time_t timer_triggertime = 0;

public:
    AppBase *currentApp = NULL;
    AppBase *app_to = NULL;
    int getAValidAppID()
    {
        return validAppID;
    }
    void increaseValidAppID()
    {
        validAppID++;
    }
    AppBase *getPtrByName(const char *appName);
    AppBase *getRealClock();
    void gotoApp(AppBase *appPtr);
    void gotoApp(const char *appName);
    void gotoAppBoot(const char *appName);
    bool recover(AppBase *home = NULL);
    void goBack();
    void showAppList(int page);
    AppBase *appSelector(bool showHidden = false);
    void update();
    String parameter = "";
    String result = "";
    void setTimer(uint32_t second, void (*fn)());
    void clearTimer();
    bool noDeepSleep = false;
    uint32_t nextWakeup = 0;
    void attachLocalEvent();
    String bootapp;
    bool luaLoaded = false;
    void loadLuaApps();
};

extern AppManager appManager;
extern AppBase *appList[128];

class LuaAppWrapper : public AppBase
{
private:
    String path;
    char _title[32];
    char _name[36];
    uint8_t _image[128];

public:
    LuaAppWrapper(const String filename, const String path);
    LuaAppWrapper()
    {
        isLuaApp = true;
        _showInList = false;
    };
    void
    initialize(const String filename, const String path);
    ~LuaAppWrapper();
    void init();
    void setup();
};

extern void newLuaApp(const char *filename);
extern void searchForLuaAPP();