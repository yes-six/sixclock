#include "AppManager.h"

class AppDemoAHT20 : public AppBase
{
private:
    /* data */
public:
    AppDemoAHT20()
    {
        name = "demoaht20";
        title = "温湿度";
        description = "AHT20示例App";
        image = NULL;
        peripherals_requested = PERIPHERALS_AHT20_BIT || PERIPHERALS_BMP280_BIT;
        _showInList = true;
    }
    void setup();
};
static AppDemoAHT20 app;

void AppDemoAHT20::setup()
{
    sensors_event_t humidity, temp;
    peripherals.aht.getEvent(&humidity, &temp); // populate temp and humidity objects with fresh data
    char buf[30];
    sprintf(buf, "温度: %g ℃\n相对湿度：%g% rH\n", temp.temperature,humidity.relative_humidity);
    Serial.println(buf);
    GUI::msgbox("传感器信息", buf);
    appManager.goBack();
}
